package mx.unitec.moviles.practica9.weather

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import mx.unitec.moviles.practica9.model.Coordenada
import mx.unitec.moviles.practica9.network.WeatherApi
import java.lang.Exception

class WeatherViewModel : ViewModel() {
    private val _response = MutableLiveData<String>()

    var coordenada = Coordenada("35","19")

    val response: LiveData<String> get() = _response

    private var viewModelJob = Job()

    private val coroutineScope =  CoroutineScope(viewModelJob + Dispatchers.Main)

    init {
        getWeatherProperties()
    }

    fun getWeatherProperties() {
        coroutineScope.launch {
            var getPropertiesDeferred = WeatherApi.retrofitService.getWeather(
                coordenada.lat, coordenada.lon)
            try {
                var result = getPropertiesDeferred.await()
                //_response.value = result
                _response.value = "name: ${result.name}, temperatura: ${result.main?.temp}"
            } catch (e:Exception){
                _response.value = "Fallo: ${e.message}"
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}